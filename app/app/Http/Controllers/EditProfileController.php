<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class EditProfileController extends Controller
{
    public function edit()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.edit', ['detail'=>$detail]);
    }

    public function editpp()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.editpp', ['detail'=>$detail]);
    }

    public function sidebar()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('partials.sidebar', ['detail'=>$detail]);
    }
}
