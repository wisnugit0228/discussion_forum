<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Profile;
use App\Models\Users;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MypostController extends Controller
{
    public function mypost(){
        $idprofile =  Auth::user()->profile->id;
        $category = Profile::get();
        
        $mypost = Post::where('profile_id', $idprofile)->first();
        $post = Post::get()->where('profile_id', $idprofile);
        

        return view('post.mypost', ['post'=>$post], ['mypost'=>$mypost]);
    }

    
}
