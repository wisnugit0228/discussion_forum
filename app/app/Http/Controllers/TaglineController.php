<?php

namespace App\Http\Controllers;

use App\Models\Tagline;
use App\Http\Requests\StoreTaglineRequest;
use App\Http\Requests\UpdateTaglineRequest;

class TaglineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Tagline::get();
        return view('profile.edit', ['tagline'=>$tagline]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTaglineRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaglineRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function show(Tagline $tagline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function edit(Tagline $tagline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTaglineRequest  $request
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaglineRequest $request, Tagline $tagline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tagline $tagline)
    {
        //
    }
}
