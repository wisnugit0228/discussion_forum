<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.view', ['detail'=>$detail]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo_profile' => 'image|mimes:jpg,png,jpeg',
            'first_name' => 'required',
            'last_name'=> 'required',
            
            'tagline' => 'required',
            'gender' => 'required',
            'bio' => 'required'
            
        ]);

        $post = Profile::find($id);

        if ($request->has('photo_profile')) {
            $patch = 'images/';
            File::delete($patch.$post->photo_profile);

            $fileName = time().'.'.$request->photo_profile->extension();
            $request->photo_profile->move(public_path('images'), $fileName);

            $post->photo_profile = $fileName;


            $post->save();

        }

        $post->first_name = $request['first_name'];
        $post->last_name = $request['last_name'];
        
        $post->tagline = $request['tagline'];
        $post->gender = $request['gender'];
        $post->bio = $request['bio'];

        $post->save();

        return redirect('Profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
