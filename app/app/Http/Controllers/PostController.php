<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use App\Models\Profile;
use App\Models\User;
use App\Models\Coment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::get();
        
        return view('post.view', ['post'=>$post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        $profile = Auth::user()->profile->id; 
        return view ('post.create', ['category'=>$category], ['profile'=>$profile]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'thumbnail' => 'required|image|mimes:jpg,png,jpeg',
            'title' => 'required',
            'content'=> 'required',
            'category_id' => 'required',
            'profile_id' => 'required'
            
        ]);


        $fileName = time().'.'.$request->thumbnail->extension();
        $request->thumbnail->move(public_path('images_content'), $fileName);

        $post = new Post;
        $post->thumbnail = $fileName;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        $post->profile_id = $request->profile_id;

        $post->save();

        return redirect('Post')->with('success', 'Berhasil Menambah Postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $post = Post::get()->where('id', $id)->first();
        $coment = DB::table('coment')->where('post_id','=',$id)->get();
        
        

        return view('post.detail', ['post'=>$post,'coment'=>$coment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $category = Category::get();
        return view ('post.edit', ['post'=>$post], ['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'thumbnail' => 'image|mimes:jpg,png,jpeg',
            'content' => 'required',
            'category_id' => 'required',
            'profile_id'=> 'required'
        ]);

        $post = Post::find($id);
        if ($request->has('thumbnail')) {
            $patch = 'images_content/';
            File::delete($patch.$post->thumbnail);

            $fileName = time().'.'.$request->thumbnail->extension();
            $request->thumbnail->move(public_path('images_content'), $fileName);

            $post->thumbnail = $fileName;


            $post->save();

        }

        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->category_id = $request['category_id'];
        $post->profile_id = $request['profile_id'];
        $post->save();

        return redirect('Post')->with('success', 'Berhasil Mengedit Kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $patch = 'images_content/';
        File::delete($patch.$post->thumbnail);

        $post->delete();

        return redirect('Post');
    }


    public function Createcoment(Request $request)
    {
        $request->request->add(['user_id'=>auth()->user()->id]);
        $coment = Coment::create($request->all());
        return redirect()->back();
    }



}
