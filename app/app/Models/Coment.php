<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coment extends Model
{
    use HasFactory;
    protected $table = 'coment';

    protected $fillable = ['coment', 'parent', 'post_id', 'user_id'];

    public function user(){
        return $this->hasMany(User::class, 'user_id');
    }

    public function post(){
        return $this->hasMany(User::class, 'post_id');
    }
}
