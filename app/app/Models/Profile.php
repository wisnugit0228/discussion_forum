<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Users;

class Profile extends Model
{
    use HasFactory;
    protected $table = 'profile';

    protected $fillable = ['photo_profile','first_name', 'last_name', 'bio', 'user_id', 'nickname', 'tagline', 'gender'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function posts(){
        return $this->hasMany(Post::class, 'profile_id');
    }
}
