<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'post';

    protected $fillable = ['title', 'thumbnail', 'content', 'category_id', 'profile_id'];

    public function kategori(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function profile(){
        return $this->belongsTo(Profile::class, 'profile_id');
    }

    public function coment(){
        return $this->hasMany(Post::class, 'post_id');
    }
}
