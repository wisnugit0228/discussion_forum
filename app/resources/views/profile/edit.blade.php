@extends('layouts.main')

@section('head')
    Profile
@endsection

@section('page')
    Edit Profile
@endsection



@section('title')
    Profile saya
@endsection

@section('content')
<form action="/Profile/{{ $detail->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group col-xl-6 col-lg-8">
      <label for="fname" class="form-label">Nama Depan</label>
      @if ($detail->first_name)
        <input type="text" class="form-control" id="fname" name="first_name"  value="{{ $detail->first_name }}" placeholder="Masukan nama depan" >
      @else
        <input type="text" class="form-control" id="fname" name="first_name"  value="-" placeholder="Masukan nama depan" >  
      @endif
      
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="lname" class="form-label">Nama Belakang</label>
        @if ($detail->last_name)
            <input type="text" class="form-control" id="lname" name="last_name" value="{{ $detail->last_name }}" placeholder="Masukan nama belakang" >
        @else
            <input type="text" class="form-control" id="lname" name="last_name" value="-" placeholder="Masukan nama belakang" >  
        @endif
        
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="gender" class="form-label">Jenis Kelamin</label>
        <select name="gender" id="gender" class="form-control" >
            @if ($detail->gender)
                    <option value="">--pilih jenis kelamin--</option>
                    <option value="pria">Pria</option>
                    <option value="wanita">Wanita</option>
                @if ($detail->gender === $detail->gender)
                    <option value="{{ $detail->gender }}" selected>{{ $detail->gender }}</option>
                @else
                    <option value="pria">Pria</option>
                    <option value="wanita">Wanita</option>
                @endif
            @else
                <option value="-" selected>-</option>
                <option value="pria">Pria</option>
                <option value="wanita">Wanita</option>
            @endif
            
            
        </select>
    </div>

    

    <div class="form-group col-xl-6 col-lg-8">
        <label for="tagline" class="form-label">Tagline</label>
        <select name="tagline" id="tagline" class="form-control">
            @if ($detail->tagline)
                @if ($detail->tagline === $detail->tagline)
                    <option value="{{ $detail->tagline }}" selected>{{ $detail->tagline }}</option>
                    <option value="Programing">Programing</option>
                    <option value="Gaming">Gaming</option>
                    <option value="Videografi">Videografi</option>
                    <option value="Fotografi">Fotografi</option>
                    <option value="Music">Music</option>
                    <option value="Student">Student</option>
                    <option value="Teacher">Teacher</option>
                    <option value="Public">Public</option>
                @else
        
                @endif
            @else
            <option value="-" selected>-</option>
            
            <option value="Programing">Programing</option>
            <option value="Gaming">Gaming</option>
            <option value="Videografi">Videografi</option>
            <option value="Fotografi">Fotografi</option>
            <option value="Music">Music</option>
            <option value="Student">Student</option>
            <option value="Teacher">Teacher</option>
            <option value="Public">Public</option>
            @endif
                
                
            
        </select>
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        
        <input type="file" class="form-control" id="photo" name="photo_profile" value="{{ $detail->photo_profile }}" style="display: none" >
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="bio" class="form-label">Bio</label>
        @if ($detail->bio)
        <textarea name="bio" id="bio" class="form-control" placeholder="masukan bio">{{ $detail->bio }}</textarea>
        @else
        <textarea name="bio" id="bio" class="form-control" placeholder="masukan bio">-</textarea>
        @endif
        
    </div>
    
    <button type="submit" class="btn btn-primary form-group">Submit</button>
  </form>
@endsection