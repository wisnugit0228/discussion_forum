@extends('layouts.main')

@section('head')
    Profile
@endsection

@section('page')
    Profile
@endsection



@section('title')
    Profile saya
@endsection


@section('content')
    

<section style="background-color: #eee;">
    
  
      <div class="row">
        <div class="col-lg-4">
          <div class="card mb-3">
            <div class="card-body text-center">
              @if ($detail->photo_profile === null)
              <img src="/asset/admin.png" alt="avatar"
              class="rounded-circle img-fluid" style="width: 150px;">
              @else
              <img src="/images/{{ $detail->photo_profile }}" alt="avatar"
              class="rounded-circle img-fluid" style="width: 150px;">
              @endif
              
                
                <h5 class="my-3">{{ Auth::user()->name }}</h5>
                
              
                @if ($detail->tagline === null)
                <p class="text-muted mb-3 bg-success"><b>#</b>-</p>
                @else
                <p class="text-muted mb-3 bg-success"><b>#</b>{{ $detail->tagline }}</p> 
                @endif
              
              
              <div class="d-flex justify-content-center mb-2">
                <a href="/profile/editpp" class="btn btn-primary">Ubah Foto Profil</a>
              </div>
            </div>
          </div>
          
        </div>
        <div class="col-lg-8">
          <div class="card mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Nama Lengkap</p>
                </div>
                <div class="col-sm-9">
                  @if ($detail->first_name === null and $detail->lasst_name === null)
                  <p class="text-muted mb-0">-</p>
                  @else
                  <p class="text-muted mb-0">{{ $detail->first_name ." ".  $detail->last_name  }}</p>
                  @endif
                  
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Email</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0">{{ Auth::user()->email }}</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Jenis Kelamin</p>
                </div>
                <div class="col-sm-9">
                  @if ($detail->gender === null)
                  <p class="text-muted mb-0">-</p> 
                  @else
                  <p class="text-muted mb-0">{{ $detail->gender }}</p> 
                  @endif
                    
                 
                    
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Bio</p>
                </div>
                <div class="col-sm-9">
                  @if ($detail->bio === null)
                  <p class="text-muted mb-0">-</p>
                  @else
                  <p class="text-muted mb-0">{{ $detail->bio }}</p>
                  @endif
                  
                  
                </div>
              </div>
              <hr>
              
            </div>
            <a href="/profile/edit" class="btn btn-warning mb-3">Edit Profile</a>
          </div>
          
          
        </div>
      </div>
    </div>
  </section>

  @endsection