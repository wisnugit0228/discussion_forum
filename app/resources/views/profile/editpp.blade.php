@extends('layouts.main')

@section('head')
    Profile
@endsection

@section('page')
    Edit foto profile
@endsection



@section('title')
    foto profile saya
@endsection

@section('content')
    

<section style="background-color: #eee;">
    
  
      <div class="row">
        <div class="container">
          <div class="card mb-3">
            <div class="card-body text-center">
              @if ($detail->photo_profile === null)
              <img src="/asset/admin.png" alt="avatar"
              class="rounded-circle img-fluid" style="width: 150px;">
              @else
              <img src="/images/{{ $detail->photo_profile }}" alt="avatar"
              class="rounded-circle img-fluid" style="width: 150px;">
              @endif
              
                <br>
              
              
              <div class="d-flex justify-content-center mb-2">
               
              </div>

            </div>
            <form action="/Profile/{{ $detail->id }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group col-xl-6 col-lg-8">
                  {{-- nama depan --}}
                    @if ($detail->first_name)
                        <input type="text" class="form-control" id="fname" name="first_name"  value="{{ $detail->first_name }}" placeholder="Masukan nama depan" style="display: none">
                    @else
                        <input type="text" class="form-control" id="fname" name="first_name"  value="-" placeholder="Masukan nama depan" style="display: none">  
                    @endif

                </div>
            
                <div class="form-group col-xl-6 col-lg-8">
                    {{-- nama belakang --}}
                    
                    @if ($detail->last_name)
                        <input type="text" class="form-control" id="lname" name="last_name" value="{{ $detail->last_name }}" placeholder="Masukan nama belakang" style="display: none" >
                    @else
                        <input type="text" class="form-control" id="lname" name="last_name" value="-" placeholder="Masukan nama belakang" style="display: none">  
                    @endif
                </div>
            
                <div class="form-group col-xl-6 col-lg-8">
                    {{-- jenis kelamin --}}
                    
                    <select name="gender" id="gender" class="form-control" style="display: none">
                    @if ($detail->gender)
                        <option value="">--pilih jenis kelamin--</option>
                        <option value="pria">Pria</option>
                        <option value="wanita">Wanita</option>
                    @if ($detail->gender === $detail->gender)
                        <option value="{{ $detail->gender }}" selected>{{ $detail->gender }}</option>
                    @else
                        <option value="pria">Pria</option>
                        <option value="wanita">Wanita</option>
                    @endif
                    @else
                        <option value="-" selected>-</option>
                        <option value="pria">Pria</option>
                        <option value="wanita">Wanita</option>
                    @endif
                        
                    </select>
                </div>
            
                
            
                <div class="form-group col-xl-6 col-lg-8">
                    {{-- tagline --}}
                    
                    <select name="tagline" id="tagline" class="form-control" style="display: none">
                    @if ($detail->tagline)
                        @if ($detail->tagline === $detail->tagline)
                            <option value="{{ $detail->tagline }}" selected>{{ $detail->tagline }}</option>
                        @else
                
                        @endif
                    @else
                        <option value="-" selected>-</option>
                    
                        <option value="Programing">Programing</option>
                        <option value="Gaming">Gaming</option>
                        <option value="Videografi">Videografi</option>
                        <option value="Fotografi">Fotografi</option>
                        <option value="Music">Music</option>
                        <option value="Student">Student</option>
                        <option value="Teacher">Teacher</option>
                        <option value="Public">Public</option>
                    @endif
                    </select>
                </div>
            
                <div class="form-group col-xl-6 col-lg-8">
                    <label for="photo" class="form-label">Pilih Foto</label>
                    <input type="file" class="form-control" id="photo" name="photo_profile" value="{{ $detail->photo_profile }}"  >
                </div>

                <div class="form-group col-xl-6 col-lg-8">
                    {{-- bio --}}
                    @if ($detail->bio)
                        <textarea name="bio" id="bio" class="form-control" placeholder="masukan bio" style="display: none">{{ $detail->bio }}</textarea>
                    @else
                        <textarea name="bio" id="bio" class="form-control" placeholder="masukan bio" style="display: none">-</textarea>
                    @endif
                </div>
                
                <button type="submit" class="btn btn-primary form-group">Submit</button>
              </form>
          </div>
          
        
      </div>
    </div>
  </section>

  @endsection