@extends('layouts.main')

@section('page')
    Welcome
@endsection

@section('link')
    Guest
@endsection

@section('title')
    Welcome
@endsection

@section('content')

<section class="jum">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ffffff" fill-opacity="1" d="M0,288L0,64L205.7,64L205.7,160L411.4,160L411.4,192L617.1,192L617.1,64L822.9,64L822.9,96L1028.6,96L1028.6,32L1234.3,32L1234.3,224L1440,224L1440,0L1234.3,0L1234.3,0L1028.6,0L1028.6,0L822.9,0L822.9,0L617.1,0L617.1,0L411.4,0L411.4,0L205.7,0L205.7,0L0,0L0,0Z"></path></svg>
    <div class="jum-content row">
        <div class="col-lg-6"> 
            <img src="{{ asset('/asset/for.png') }}" class="img-fluid" alt="...">
        </div>
        
        <div class="col-lg-6">
            <h3>Selamat Datang di Culture Diskusi</h3>
        <p>
            <b>Culture Diskusi</b> adalah website diskusi dalam bentuk forum tanya jawab, dimana di website ini kalian bisa
            memposting pertanyaan maupun menjawab pertanyaan dari pengguna lainnya. tentunya website ini memiliki segudang informasi
            dan juga tentunya akan menambah dan memperluas pengetahuan anda. <br>
            yuk gabung sekarang dan keluarkan semua pertanyaan - pertanyaan anda dan kita bahas langsung secara berdiskusi!!!
        </p>
        <a href="/login" class="btn btn-primary">Gabung Sekarang</a>
        </div>

        
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ffffff" fill-opacity="1" d="M0,288L0,64L205.7,64L205.7,160L411.4,160L411.4,192L617.1,192L617.1,64L822.9,64L822.9,96L1028.6,96L1028.6,32L1234.3,32L1234.3,224L1440,224L1440,320L1234.3,320L1234.3,320L1028.6,320L1028.6,320L822.9,320L822.9,320L617.1,320L617.1,320L411.4,320L411.4,320L205.7,320L205.7,320L0,320L0,320Z"></path></svg>
    
  </section>


    
@endsection