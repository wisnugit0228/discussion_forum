@extends('layouts.main')

@section('head')
    Kategori
@endsection

@section('page')
    Edit Kategori
@endsection



@section('title')
    Edit Kategori
@endsection

@section('content')
<form action="/Category/{{ $category->id }} " method="POST" >
    @csrf
    @method('put')
      <div class="form-group col-xl-6 col-lg-8">
          <label for="name">Nama Kategori</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ $category->name }}" placeholder="Masukan kategori baru">
      </div>
      @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group col-xl-6 col-lg-8">
          <label for="descript">Deskripsi</label>
          <textarea class="form-control" id="descript" name="descript" rows="3">{{ $category->descript }}</textarea>
          
      </div>
      @error('descript')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
      
      
    </form>
@endsection