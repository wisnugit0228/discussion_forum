@extends('layouts.main')
@section('head')
    Kategori
@endsection

@section('page')
    Tambah Kategori
@endsection



@section('title')
    Tambah Kategori baru
@endsection

@section('content')
<form action="/Category" method="POST" >
    @csrf
      <div class="form-group col-xl-6 col-lg-8">
          <label for="name">Nama Kategori</label>
          <input type="text" class="form-control" name="name" id="name"  placeholder="Masukan kategori baru">
      </div>
      @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group col-xl-6 col-lg-8">
          <label for="descript">Deskripsi</label>
          <textarea class="form-control" id="descript" name="descript" rows="3"></textarea>
          
      </div>
      @error('descript')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
      
      
    </form>
@endsection