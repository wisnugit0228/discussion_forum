@extends('layouts.main')

@section('head')
    Kategori
@endsection

@section('page')
    Detail
@endsection

@section('title')
    Detail Kategori
@endsection

@section('content')
    <h1> {{ $post->name}} </h1>
    <p> {{ $post->descript }} </p><br>
    
    <a href="/Category" >
        <button type="button" class="btn btn-primary mb-3">Kembali</button>
      </a>
@endsection