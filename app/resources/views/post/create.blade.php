@extends('layouts.main')

@section('head')
    my post
@endsection

@section('page')
    Buat Postingan Baru
@endsection



@section('title')
    Postinagn Baru
@endsection

@section('content')
<form action="/Post" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group col-xl-6 col-lg-8">
        <label for="thumbnail" class="form-label">Thumbnail</label>
        <input type="file" class="form-control" id="thumbnail" name="thumbnail"  >
    </div>

      <div class="form-group col-xl-6 col-lg-8">
          <label for="title">Judul</label>
          <input type="text" class="form-control" name="title" id="title"  placeholder="Masukan Judul">
      </div>
      @error('title')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group col-xl-6 col-lg-8">
          <label for="content">Konten</label>
          <textarea class="form-control" id="content" name="content" rows="3"></textarea>
          
      </div>
      @error('content')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror

      <div class="form-group col-xl-6 col-lg-8">
        <label for="kategori">Tambah Kategori Baru</label>
        <select name="category_id" id="kategori" class="form-control">
           <option value="">--Pilih Kategori--</option>
            @forelse ($category as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                
            @empty
                <option value="">Tidak ada Pilihan</option>
                
            @endforelse
        </select>
      </div>
      @error('category_id')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror

      <div class="form-group col-xl-6 col-lg-8">
          
          <input type="hidden" class="form-control" name="profile_id" id="profile_id" value="{{ $profile }}" >
      </div>
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
      
      
</form>
@endsection