@extends('layouts.main')

@section('head')
    Dashboard
@endsection

@section('page')
    Dashboard
@endsection



@section('title')
    selamat datang
@endsection

@section('content')
    

    <div class="container-fluid">
        <div class="row">
            
            @foreach ($post as $item)
            <div class="col-lg-4">
                <div class="card bg-light">
                  <a href="#"><img src="../images_content/{{ $item->thumbnail }}" class="card-img-top" alt="..." /></a>
                  <div class="card-body">
                    <div class="activity-update">
                        <div class="update-item">
                            <cite>
                                <a href="" class="bp-tooltip" data-bp-tooltip-pos="up" data-bp-tooltip="John">
                                @if ($item->profile->photo_profile = $item->profile->photo_profile)
                                    <img src="../images/{{ $item->profile->photo_profile }}" class="avatar user-2-avatar avatar-40 photo" width="40" height="40" alt="Profile photo of {{ $item->profile->user->name }}"> </a>
                                @else
                                    <img src="../asset/admin.png" class="avatar user-2-avatar avatar-40 photo" width="40" height="40" alt="Profile photo of {{ $item->profile->user->name }}"> </a>
                                @endif
                            </cite>
                        </div>
                        <div class="bp-activity-info">
                            <p><a href=""> {{ $item->profile->user->name }}</a> <br> post at <a href="" class="view activity-time-since"><span class="time-since">{{ $item->created_at->diffForHumans() }}</span></a></p>
                        </div>
                    </div>
                    
                    <p>{{ $item->title }}</p>
                    <a href="/Post/{{ $item->id }}" class="btn btn-primary">Lihat</a>
                  </div>
                </div>
            </div>
            @endforeach
            
            
            

        </div>
        

    </div>
@endsection
