@extends('layouts.main')

@section('head')
    Post
@endsection

@section('page')
    Post
@endsection



@section('title')
    Detail Postingan
@endsection

@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    
                 <aside id="bp_latest_activities-8" class="widget activity_update bp-latest-activities buddypress">
                        <div class="activity-list item-list">
                            <div class="activity-update">
                                <div class="update-item">
                                    <cite>
                                        <a href="" class="bp-tooltip" data-bp-tooltip-pos="up" data-bp-tooltip="John">
                                            <img src="../images/{{ $post->profile->photo_profile }}" class="avatar user-2-avatar avatar-40 photo" width="40" height="40" alt="Profile photo of {{ $post->profile->user->name }}"> </a>
                                    </cite>
                                <div class="bp-activity-info">
                                        <p><a href=""> {{ $post->profile->user->name }}</a> post at <a href="" class="view activity-time-since"><span class="time-since">{{ $post->created_at->diffForHumans() }}</span></a></p>
                            </div>
                        </div>
                                                 
                </aside> 

                <div class="title">
                    <h5>{{ $post->title }}</h5>
                </div>
                
                <div class="img-content col-sm-6">
                    <img src="../images_content/{{ $post->thumbnail }}" class="img-fluid" alt="...">
                </div>

                <div class="content">
                    <p class="text-justify">{{ $post->content }}</p>
                </div>

                <form action="/Post/{{ $post->id }}" method="POST">
                    @csrf
                    <div class="form-group col-xl-6 col-lg-8">
                        <label for="content">Komentar</label>
                        <textarea class="form-control" id="content" name="coment" rows="1"></textarea>
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <input type="hidden" name="parent" value="0">
                        <br>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>

                @foreach ($coment as $cmnt)
                <div class="coment">
                    <a href="" class="bp-tooltip" data-bp-tooltip-pos="up" data-bp-tooltip="John">
                        <img src="../images/" class="avatar user-2-avatar avatar-40 photo" width="40" height="40" alt=""> coment </a>
                        <p>{{ $cmnt->coment }}</p>
                </div>
                @endforeach
               
                
                
                
                        
                           
                </div>
            </div>
        </div>
    </div>
@endsection