<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\ComentController;
use App\Http\Controllers\MypostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TaglineController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\EditProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [GuestController::class, 'view']);

Route::middleware(['auth'])->group(function () {

Route::resource('Category', CategoryController::class);

Route::resource('Post', PostController::class);

Route::resource('Profile', ProfileController::class)->only(['index', 'update']);

//edit profile
Route::get('/profile/edit', [EditProfileController::class, 'edit']);

//edit photo profile
Route::get('/profile/editpp', [EditProfileController::class, 'editpp']);

//Route::resource('Tagline', TaglineController::class)->only(['index']);

//view mypost
Route::get('/mypost', [MypostController::class, 'mypost']);

Route::post('/Post/{post_id}', [PostController::class, 'Createcoment']);

// Route::get('/Post/{post_id}', [ComentController::class, 'index']);

// Route::resource('/Post/{post_id}', ComentController::class)->only(['index', 'store', 'create']);

    
});






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
